import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/Login.vue';
import Workers from '../views/Workers.vue';
import WorkerPage from '../views/WorkerPage.vue';
import Profile from '../views/Profile.vue';
import Forgot from '../views/Forgot.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
  },
  {
    path: '/workers',
    name: 'workers',
    component: Workers,
    meta: { requiresAuth: true },
  },
  {
    path: '/workers/:id',
    props: true,
    component: WorkerPage,
    meta: { requiresAuth: true },
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile,
    meta: { requiresAuth: true },
  },
  {
    path: '/forgot',
    name: 'forgot',
    component: Forgot,
    meta: { noAuth: true },
  },
];

const router = new VueRouter({ routes });

export default router;
