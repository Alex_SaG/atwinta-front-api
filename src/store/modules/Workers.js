import axios from 'axios';
import moment from 'moment';

const state = {
  workersList: [],
  curPage: 1,
  lastPage: 1,
  curWorker: null,
};

const getters = {
  workersList: () => state.workersList,
  curPage: () => state.curPage,
  lastPage: () => state.lastPage,
  worker: () => state.curWorker,
};

const mutations = {
  pageChange: (context, num) => { state.curPage = num; },
  getWorkerPage: (context, worker) => { state.curWorker = worker; },
  getWorkers: (context, workersList) => {
    state.workersList = workersList.data;
    state.lastPage = workersList.last_page;
  },
};

const actions = {
  getWorkers(context) {
    axios.get(`/workers?page=${context.state.curPage}`, { headers: { Authorization: `Bearer  ${context.rootState.auth.token}` } })
      .then((response) => {
        context.commit('getWorkers', response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  },
  pageChange(context, formData) {
    context.commit('pageChange', formData.num);
    context.dispatch('getWorkers');
  },
  getWorkerPage(context, formData) {
    axios.get(`/workers/${formData.id}`, { headers: { Authorization: `Bearer  ${context.rootState.auth.token}` } })
      .then((response) => {
        response.data.worker.adopted_at = moment(response.data.worker.adopted_at).format('DD/MM/YYYY');
        context.commit('getWorkerPage', response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
