import axios from 'axios';

const state = {
  user: {},
};

const getters = {
  user: () => state.user,
};

const mutations = {
  userUpdate: (context, user) => {
    state.user = user;
  },
};

const actions = {
  updateProfile(context, formData) {
    const data = {
      about: formData.about,
      github: formData.github,
      city: formData.city,
      telegram: formData.telegram,
      phone: formData.phone.replace(/\D+/g, ''),
      birthday: formData.birthday,
    };

    return new Promise((resolve, reject) => {
      if (data.phone.match(/\d/g).length !== 10) {
        reject(new Error('Не верно введен телефон'));
      } else {
        axios.post('/user', data, { headers: { Authorization: `Bearer  ${context.rootState.auth.token}` } })
          .then((response) => {
            context.commit('userUpdate', response.data);
            resolve('Данные сохранены');
          })
          .catch((error) => {
            console.error(error);
            reject(error);
          });
      }
    });
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
