import axios from 'axios';

const state = {
  token: localStorage.getItem('access_token') || null,
};

const getters = {
  loggedIn: () => state.token !== null,
};

const mutations = {
  getToken: (context, token) => { state.token = token; },
  removeToken: () => localStorage.clear(),
};

const actions = {
  getToken(context, formData) {
    return new Promise((resolve, reject) => {
      if (formData.email.length < 3 || !formData.email.includes('@') || !formData.email.includes('.')) {
        reject(new Error('Не верно введен E-mail'));
      } else if (formData.password.length < 3) {
        reject(new Error('Не верно введен пароль'));
      } else {
        axios.post('/auth/login', {
          email: formData.email,
          password: formData.password,
        }, { headers: { Authorization: `Bearer  ${state.token}` } })
          .then((response) => {
            const { token } = response.data;
            localStorage.setItem('access_token', token);
            context.commit('getToken', token);
            resolve(response);
          })
          .catch((error) => {
            console.error(error);
            reject(error);
          });
      }
    });
  },
  userAuth(context) {
    axios.get('/user', { headers: { Authorization: `Bearer  ${state.token}` } })
      .then((response) => {
        context.commit('userUpdate', response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  },
  restorePassword(context, formData) {
    return new Promise((resolve, reject) => {
      if (formData.email.length < 3 || !formData.email.includes('@') || !formData.email.includes('.')) {
        reject(new Error('Не верно введен E-mail'));
      } else {
        axios.post('/auth/restore', { email: formData.email })
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            console.error(error);
            reject(error);
          });
      }
    });
  },
  loggedOut: (context) => context.commit('removeToken'),
};

export default {
  state,
  getters,
  actions,
  mutations,
};
