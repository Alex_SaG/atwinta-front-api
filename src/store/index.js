import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

import auth from './modules/auth';
import workers from './modules/workers';
import profile from './modules/profile';

Vue.use(Vuex);

axios.defaults.baseURL = 'http://test.atwinta.ru/api/v1';

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth,
    workers,
    profile,
  },
});
